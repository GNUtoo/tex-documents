#!/usr/bin/awk -f
BEGIN {FS="\t"}
{
  printf("%s ",$1) ;

  # The maximum instruction length that the produced
  # disassembly had, tuning it by hand permits to be
  # more compact while not spending time to do it automatically
  padding=14

  # Here we want to strip out the extra spaces produced by objdump
  # we still want to pad it to align it well with the instruction decoding
  for(i=1;i<=length($2); i++){
    c=substr($2,i,1);
    if(c==" "){
    } else{
      printf("%s",c);
      padding--;
    }
  };
  # Add the padding
  for(i=0;i<padding;i++){
    printf(" ");
  }

  # Prefix with one whitespace,
  printf(" ");
  # and colapses multiples countinuous whitespaces in one
  for(i=1;i<=length($3); i++){
    whitespaces=0
    c=substr($3,i,1);
    if(c==" "){
      whitespace++;
    } else {
      if(whitespace>0){
        printf(" ");
        whitespace=0;
      }
      printf("%s", c);
    }
  }
  printf("\n");
}
