TEXFLAGS ?= -halt-on-error -output-format pdf -output-directory output
TITLE = boot-freedom
LOCATION ?= Coliberator

.PHONY: all clean test $(TITLE)_en.pdf medias
all: $(TITLE)_en.pdf

output:
	mkdir output

medias: \
	output/coreinfo-cbfs.jpeg \
	output/coreinfo-cbmemc.jpeg \
	output/coreinfo-cpuinfo.jpeg \
	output/grub-cmdline.jpeg \
	output/grub-menu.jpeg \
	output/HP.jpeg \
	output/memtest86.jpeg \
	output/nvramcui.jpeg \
	output/SeaBIOS.jpeg \
	output/SeaBIOS-menu.jpeg \
	output/SOIC-16.jpeg \
	output/Thinkpad.jpeg \
	output/tint.jpeg \
	output/WiFi.jpeg \
	output/X200.jpeg \
	output/Motherboard_diagram.jpeg \
	output/MicroATX_Motherboard_with_AMD_Athlon_Processor_2_Digon3.jpeg \
	output/Desktop_computer_clipart_-_Yellow_theme.jpeg \
	output/Libreboot_on_Lenovo_Thinkpad_x200.jpeg \
	output/BUG_Group_-_Hiro_P_edition.jpeg \
	output/WSON-8.jpeg \
	output/Laptop_Acrobat_Model_NBD_486C,_Type_DXh2_-_California_Micro_Devices_CMD_9324_on_motherboard-9749.jpeg \
	output/WD_Caviar_Green_WD10EADS_-_Controller_-_Winbond_25X20ALNIG-91999.jpeg \

$(TITLE)_en.pdf: clean output/disassembly.txt medias
	pdflatex $(TEXFLAGS) \
		'\newcommand{\location}{$(LOCATION)}\providecommand\locale{en}\input{$(TITLE).tex}'
	pdflatex $(TEXFLAGS) \
		'\newcommand{\location}{$(LOCATION)}\providecommand\locale{en}\input{$(TITLE).tex}'

test: $(TITLE)_en.pdf
	xdg-open output/$(TITLE).pdf

output/disassembly.txt: output
	$(CC) -include/usr/include/stdio.h source.c -o output/binary
	strip output/binary
	objdump --section .text -d output/binary > output/text_section.txt
	grep "^ " output/text_section.txt > output/disassembly.txt
	sed -i 's#^ ##' output/disassembly.txt # Remove beginning whitespace
	./assembly.awk output/disassembly.txt > output/assembly.txt

output/%.jpeg: output medias/%.xcf
	convert medias/$(patsubst %.jpeg,%.xcf,$(@F)) $@

output/%.jpeg: output medias/%.svg
	convert medias/$(patsubst %.jpeg,%.svg,$(@F)) $@

output/%.jpeg: output medias/%.jpg
	cp medias/$(patsubst %.jpeg,%.jpg,$(@F)) $@

clean:
	rm -rf output
